#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Wed Feb  5 13:29:02 2020

@author: m16016813
"""
import unittest

## Tutoriel-Partie 2

### 2) 

#### La fonction test_crashing_test renvoie une erreur car elle propose
#### de vérifier si 6 est égal à 6 /0. Or en mathématiques, la division 
#### par 0 est interdite. Et la fonction test_choice_erroneous_test renvoie 
#### un échec car l'élément pris au hasard dans la liste self.liste 
#### n'a pas été trouvé dans le tuple ('a', 'b', 'c')  

#### Un code peut lever une exception avec soit un bloc try ou si on 
#### utilise la bibliothèque unittest, on peut utiliser la méthode 
#### assertRaises. 

#### setUp et TearDown sont des méthodes qui vont pouvoir être utilisées 
#### avant ou après chaque test. setUp est une méthode qui va permettre 
#### d'initialiser un test, si une anomalie dans le code est détectée
#### après cette méthode, celle-ci sera considérée comme une erreur. 
#### tearDown est une méthode qui est appelée une fois que la méthode 
#### setUp ait été exécutée. Par défaut, l'appellation de cette méthode
#### ne lance aucune nouvelle opération. 


## Exercice 
### 1.1 

class Pile(object):
    def __init__(self): 
        self.pile = []
    
    def creerPileVide(self):
        self.pile = []

    def pileVide(self):
        return self.pile == []

    def empiler(self, e):
        self.pile.append(e)

    def depiler(self):
        if self.pileVide():
           raise IndexError("attempt to pop from empty stack")
        return self.pile.pop()

    def sommet(self):
        if self.pileVide():
            raise IndexError("attempt to read top of empty stack")
        return self.pile[-1]
    
class Test(unittest.TestCase): 
    
    def setUp(self): 
        self.pile = Pile()
        
    def test_depiler(self): 
        self.assertRaises(IndexError, self.pile.depiler)
        
    def test_sommet_pile(self): 
        self.assertRaises(IndexError, self.pile.sommet)
        
    def tearDown(self): 
        pass
    
### 1.2
   
    def test_creation_pile(self): 
        self.pile = Pile()
        self.assertRaises(ValueError, self.pile.pileVide) 
        
    def test_empilement_resultat(self): 
        self.pile = Pile() 
        self.pile.empiler(1)
        self.assertRaises(ValueError, self.pile.pileVide)
        
    def test_empil_depil(self): 
        self.pile = Pile() 
        self.pile.empiler(1)
        self.pile.depiler() 
        self.assertRaises(ValueError, self.pile.pileVide)
        
    def test_value_sommet_pile(self): 
        self.pile = Pile()
        element = 1
        self.pile.empiler(element)
        element = 2
        self.pile.empiler(element) 
        self.assertEqual(self.pile.sommet, element)
     
